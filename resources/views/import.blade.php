<html>
    <head>
        <style type="text/css">
            .menu_link{
                margin:10px;
            }
        </style>
    </head>
    <body>
        <h1>Import CSV</h1>
        <form action="" method="post" enctype='multipart/form-data'>
            @csrf
            <table>
                <tr>
                    <td colspan="3" align="center">
                        <a href="{{ env('APP_URL') }}" class="menu_link">Import</a>
                        <a href="{{ env('APP_URL') }}/report1" class="menu_link">Report 1</a>
                        <a href="{{ env('APP_URL') }}/report2" class="menu_link">Report 2</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">{{ $message }}</td>
                </tr>
                <tr>
                    <td width="200px"></td>
                    <td>File</td>
                    <td><input type="file" name="csv_file"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><input type="submit" name="go" value="Go"/></td>
                </tr>
                <tr>
                    <td colspan="3">Assumptions : 
                        <ul>
                            <li>php.ini - upload_max_filesize=400M</li>
                            <li>php.ini - post_max_size=400M</li>
                            <li>db server - localhost</li>
                            <li>db name - machine_test_icloud</li>
                        </u> 
                    </td>
                </tr>
                <tr>
                    <td colspan="3">Improvements : 
                        <ul>
                            <li>Entries missing admission no. and other some data: 670946,753671,781437,781640</li>
                            <li>A table created, 'fee_types_list' to get 'Seq_id' for table, 'Fee_types'</li>
                        </u> 
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>