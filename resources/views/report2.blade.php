<html>
    <head>
        <style type="text/css">
            .menu_link{
                margin:10px;
            }
        </style>
    </head>
    <body>
        <h1>Report 1</h1>
        <table>
            <tr>
                <td colspan="3" align="center">
                    <a href="{{ env('APP_URL') }}" class="menu_link">Import</a>
                    <a href="{{ env('APP_URL') }}/report1" class="menu_link">Report 1</a>
                    <a href="{{ env('APP_URL') }}/report2" class="menu_link">Report 2</a>
                </td>
            </tr>
            <tr>
                <td width="200px"></td>
                <td>File</td>
                <td><input type="file" name="csv_file"/></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><input type="submit" name="go" value="Go"/></td>
            </tr>
        </table>
    </body>
</html>