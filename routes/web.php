<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaymentDataController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PaymentDataController::class, 'show_import'])->name('show.import');
Route::post('/', [PaymentDataController::class, 'import'])->name('import');

Route::get('/report1', [PaymentDataController::class, 'report1'])->name('show.report1');
Route::get('/report2', [PaymentDataController::class, 'report2'])->name('show.report2');
