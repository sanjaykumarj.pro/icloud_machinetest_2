<?php

namespace App\Http\Controllers;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentDataController extends Controller
{
    public function show_import()
    {
        $message = "Please select a csv file and continue.";
        return view('import', compact('message'));
    }
    
    public function import()
    {
        $message = "Please select a csv file and continue.";
        
        if (!empty($_FILES['csv_file'])) {

            // $cons= mysqli_connect("localhost", "root","","machine_test_icloud") or die(mysqli_error());
        
            // mysqli_query($cons, "LOAD DATA LOCAL INFILE '".CommonHelper::formatt($_FILES['csv_file']['tmp_name'])."' INTO TABLE payment_data_from_csv FIELDS TERMINATED by ',' LINES TERMINATED BY '\n'")or die(mysqli_error($cons));

            DB::insert("LOAD DATA LOCAL INFILE '".CommonHelper::formatt($_FILES['csv_file']['tmp_name'])."' INTO TABLE payment_data_from_csv FIELDS TERMINATED by ',' LINES TERMINATED BY '\n'");
        
            $message = "Successfully imported... ".$message;
        }
        else $message = "Failed! ".$message;

        return view('import', compact('message'));
    }
    
    public function report1()
    {
        $reportData=[];
        return view('report1', compact('reportData'));
    }
    
    public function report2()
    {
        $reportData=[];
        return view('report2', compact('reportData'));
    }
}