<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeeTypesListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fee_types_list')->insert(
            [
                ['fee_type_item' => 'Tuition Fee']
                ,['fee_type_item' => 'Exam Fee']
                ,['fee_type_item' => 'Security Fee']
                ,['fee_type_item' => 'Ajustable_Excess_Amount']
                ,['fee_type_item' => 'Adjusted_Amount']
                ,['fee_type_item' => 'Reckecking/Scrutiny Fee']
                ,['fee_type_item' => 'Fine Fee']
                ,['fee_type_item' => 'Adjustable Excess Fee']
                ,['fee_type_item' => 'Tuition Fee (Back Paper)']
                ,['fee_type_item' => 'Tuition Fee (IBM ClaCCeC)']
                ,['fee_type_item' => 'Exam Fee (CemeCter)']
                ,['fee_type_item' => 'Library Fine Fee']
                ,['fee_type_item' => 'Exam Fee (Back Paper)']
                ,['fee_type_item' => 'Degree/Convocation/Certificate fee']
                ,['fee_type_item' => 'Library BookC Recieved']
                ,['fee_type_item' => 'Sport Activity Received']
                ,['fee_type_item' => 'Training & Certification Fee']
                ,['fee_type_item' => 'Exam Fee Debarred']
                ,['fee_type_item' => 'Tuition Fee Debarred']
                ,['fee_type_item' => 'Exam Fees Back Paper']
                ,['fee_type_item' => 'Exam Fee (Letral Deploma)']
                ,['fee_type_item' => 'Exam Fees Debarred Paper']
                ,['fee_type_item' => 'Tution Fees debarred paper']
                ,['fee_type_item' => 'Convocation Fee Head']
                ,['fee_type_item' => 'Student ID Fee']
                ,['fee_type_item' => 'Library Books Recieved']
                ,['fee_type_item' => 'Special Backlog fee']
                ,['fee_type_item' => 'Registration Fee']
                ,['fee_type_item' => 'Tuition Fee (IBM Classes)']
                ,['fee_type_item' => 'Registration FIne Even Sem']
                ,['fee_type_item' => 'Online Registration Fine odd Sem']
                ,['fee_type_item' => 'Revaluation Fee']
                ,['fee_type_item' => 'Rechecking Fee']
                ,['fee_type_item' => 'Indisciplinary Fine']
                ,['fee_type_item' => 'Exam Fee ET Eligibilty']
                ,['fee_type_item' => 'Online Registration Fine even Sem']
                ,['fee_type_item' => 'Misc Exam Fees Back Paper']
                ,['fee_type_item' => 'Exam Fee (Semester)']
                ,['fee_type_item' => 'Thesis Fees']
                ,['fee_type_item' => 'OTHER FEES']
                ,['fee_type_item' => 'Tuition Fees']
                ,['fee_type_item' => 'Exam Fees']
                ,['fee_type_item' => 'Excess Amount']
                ,['fee_type_item' => 'Hostel & Mess Fee']
                ,['fee_type_item' => 'Other Fee']
                ,['fee_type_item' => 'Degree Fees']
                ,['fee_type_item' => 'Registration FIne Odd Sem']
                ,['fee_type_item' => 'Degree Fee']
                ,['fee_type_item' => 'Student ID Fee MISC']
            ]
        );
    }
}
