<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branches')->insert(
            [
                ['name' => 'School of Business']
                , ['name' => 'School of Finance & Commerce']
                , ['name' => 'School of Mechanical Engineering']
                , ['name' => 'School of Computing Science & Engineering']
                , ['name' => 'School of Electrical Electronics & Communication Engineering']
                , ['name' => 'School of Civil Engineering']
                , ['name' => 'School of Basic & Applied Sciences']
                , ['name' => 'School of Library & Information Science']
                , ['name' => 'School of Humanities & Social Sciences']
                , ['name' => 'School of Medical and Allied Sciences']
                , ['name' => 'School of Media & Communication Studies']
                , ['name' => 'School of Clinical Research & Healthcare']
                , ['name' => 'School of Law']
                , ['name' => 'School of Hospitality']
                , ['name' => 'School of Nursing']
                , ['name' => 'School of Chemical Engineering']
                , ['name' => 'School of Architecture']
                , ['name' => 'School of Education']
                , ['name' => 'Gs Polytechnic']
                , ['name' => 'School of Logistics & Aviation Management']
                , ['name' => 'School Of Biological And Biomedical Sciences']
                , ['name' => 'School of Agriculture']
                , ['name' => 'School of Design']
                , ['name' => 'School of Biotechnology']
            ]
        );
    }
}
