<?php

namespace Database\Seeders;

use App\Models\Branch;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeeCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fee_categories')->insert($this->prepare_seed());
    }

    private function prepare_seed()
    {
        $all_branches = Branch::all();
        $return_array = [];

        foreach($all_branches as $branch)
        {
            $return_array[] = ['br_id'=>$branch->id, 'fee_category'=>'General'];
            $return_array[] = ['br_id'=>$branch->id, 'fee_category'=>'SAARC NRI'];
            $return_array[] = ['br_id'=>$branch->id, 'fee_category'=>'Non SAARC NRI'];
        }

        return $return_array;
    }
}
