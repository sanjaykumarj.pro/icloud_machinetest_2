<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FinancialTransDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("INSERT INTO financial_trans_details(financialTranId, moduleId, amount, headId, crdr, brid, head_name) (
            SELECT id
                , 1
                , paid_amount
                , (SELECT id FROM `fee_types_list` WHERE fee_type_item=T1.fee_head)
                , 'D'
                , (SELECT id FROM `branches` WHERE name=T1.faculty)
                , fee_head
            FROM payment_data_from_csv T1)");
    }
}
