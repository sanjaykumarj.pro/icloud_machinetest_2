<?php

namespace Database\Seeders;

use App\Models\Branch;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeeCollectionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fee_collection_types')->insert($this->prepare_seed());
    }
    private function prepare_seed()
    {
        $all_branches = Branch::all();
        $return_array = [];

        foreach($all_branches as $branch)
        {
            $return_array[] = ['br_id'=>$branch->id, 'collectionhead'=>'Academic', 'collectiondesc'=>'Academic'];
            $return_array[] = ['br_id'=>$branch->id, 'collectionhead'=>'AcademicMisc', 'collectiondesc'=>'Academic Misc'];
            $return_array[] = ['br_id'=>$branch->id, 'collectionhead'=>'Hostel', 'collectiondesc'=>'Hostel'];
            $return_array[] = ['br_id'=>$branch->id, 'collectionhead'=>'HostelMisc', 'collectiondesc'=>'Hostel Misc'];
            $return_array[] = ['br_id'=>$branch->id, 'collectionhead'=>'Transport', 'collectiondesc'=>'Transport'];
            $return_array[] = ['br_id'=>$branch->id, 'collectionhead'=>'TransportMisc', 'collectiondesc'=>'Transport Misc'];
        }

        return $return_array;
    }
}
