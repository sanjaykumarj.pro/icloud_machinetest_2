<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert(
            [
                ['name' => 'due', 'module_id'=>'1']
                , ['name' => 'Academic Misc', 'module_id'=>'11']
                , ['name' => 'Hostel', 'module_id'=>'2']
                , ['name' => 'Hostel Misc', 'module_id'=>'22']
                , ['name' => 'Transport', 'module_id'=>'3']
                , ['name' => 'Transport Misc', 'module_id'=>'33']
            ]
        );
    }
}
