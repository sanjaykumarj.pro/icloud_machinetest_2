<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EntrymodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('entry_modes')->insert(
            [
                ['name' => 'due', 'crdr'=>'D', 'entrymodeno'=>0]
                , ['name' => 'duerev', 'crdr'=>'C', 'entrymodeno'=>12]
                , ['name' => 'scholarship', 'crdr'=>'C', 'entrymodeno'=>15]
                , ['name' => 'scholarshiprev/revconsession', 'crdr'=>'D', 'entrymodeno'=>16]
                , ['name' => 'consession', 'crdr'=>'C', 'entrymodeno'=>15]
                , ['name' => 'RCPT', 'crdr'=>'C', 'entrymodeno'=>0]
                , ['name' => 'REVRCPT', 'crdr'=>'D', 'entrymodeno'=>0]
                , ['name' => 'JV', 'crdr'=>'C', 'entrymodeno'=>14]
                , ['name' => 'RevJV', 'crdr'=>'D', 'entrymodeno'=>14]
                , ['name' => 'PMT', 'crdr'=>'D', 'entrymodeno'=>1]
                , ['name' => 'REVPMT', 'crdr'=>'C', 'entrymodeno'=>1]
                , ['name' => 'Fundtransfer', 'crdr'=>'Positive and Negative', 'entrymodeno'=>1]
            ]
        );
    }
}
