<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_trans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('moduleId')->unsigned();
            $table->bigInteger('admno')->unsigned();
            $table->float('amount', 12,2);
            $table->string('crdr', 100);
            $table->string('tranDate', 50);
            $table->string('acadYear', 50);
            $table->string('entry_mode', 10);
            $table->bigInteger('voucherno')->unsigned();
            $table->bigInteger('brid')->unsigned()->nullable();
            $table->tinyText('tinyInteger')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_trans');
    }
}
