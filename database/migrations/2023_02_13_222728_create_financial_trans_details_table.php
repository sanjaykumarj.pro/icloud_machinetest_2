<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialTransDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_trans_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('financialTranId')->unsigned();
            $table->bigInteger('moduleId')->unsigned();
            $table->float('amount', 12,2);
            $table->bigInteger('headId')->unsigned()->nullable();
            $table->string('crdr', 100);
            $table->bigInteger('brid')->unsigned()->nullable();
            $table->string('head_name', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_trans_details');
    }
}
