<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentDataFromCsv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_data_from_csv', function (Blueprint $table) {
            $table->id();
            $table->string('date', 30);
            $table->string('academic_year', 30);
            $table->string('session', 30);
            $table->string('alloted_category', 30);
            $table->string('voucher_type', 30);
            $table->string('voucher_no', 30);
            $table->string('roll_no', 50);
            $table->string('admno', 50);
            $table->string('status', 50);
            $table->string('fee_category', 30);
            $table->string('faculty', 200);
            $table->string('program', 200);
            $table->string('department', 100);
            $table->string('batch', 200);
            $table->string('receipt_no', 100);
            $table->string('fee_head', 100);
            $table->float('due_amount', 12,2);
            $table->float('paid_amount', 12,2);
            $table->float('concession_amount', 12,2);
            $table->float('scholarship_amount', 12,2);
            $table->float('reverse_concession_amount', 12,2);
            $table->float('write_off_amount', 12,2);
            $table->float('adjusted_amount', 12,2);
            $table->float('refund_amount', 12,2);
            $table->float('fund_transfer_amount', 12,2);
            $table->string('remarks', 1000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_data_from_csv');
    }
}
